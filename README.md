# pi_bootgen



## Getting started
The purpose of this repo is to show how to add a layer to yocto for a rapsberry pi. The image run on qemu so no needs to have actual hardware.

## Clone this repo
`git clone https://gitlab.com/alexcaron/pi_bootgen.git
cd pi_bootgen`

## Install required tools by bitbake
```
sudo apt-get install chrpath diffstat gawk
```
## Clone metadata layers for Raspberry
```
git clone -b dunfell git://git.yoctoproject.org/poky.git
git clone -b dunfell git://git.yoctoproject.org/meta-raspberrypi.git
git clone -b dunfell git://git.openembedded.org/meta-openembedded
```
## init build enviroment
```
source poky/oe-init-build-env
```
## Add layer to yocto
```
bitbake-layers add-layer ../meta-openembedded/meta-oe
bitbake-layers add-layer ../meta-openembedded/meta-python
bitbake-layers add-layer ../meta-openembedded/meta-multimedia
bitbake-layers add-layer ../meta-openembedded/meta-networking
bitbake-layers add-layer ../meta-raspberrypi
bitbake-layers add-layer ../meta-bootgen
bitbake-layers show-layers

layer                 path                                      priority
==========================================================================
meta                  /home/alex/pi_bootgen/yocto/poky/meta     5
meta-poky             /home/alex/pi_bootgen/yocto/poky/meta-poky  5
meta-yocto-bsp        /home/alex/pi_bootgen/yocto/poky/meta-yocto-bsp  5
meta-oe               /home/alex/pi_bootgen/yocto/meta-openembedded/meta-oe  6
meta-python           /home/alex/pi_bootgen/yocto/meta-openembedded/meta-python  7
meta-multimedia       /home/alex/pi_bootgen/yocto/meta-openembedded/meta-multimedia  6
meta-networking       /home/alex/pi_bootgen/yocto/meta-openembedded/meta-networking  5
meta-raspberrypi      /home/alex/pi_bootgen/yocto/meta-raspberrypi  9
meta-bootgen          /home/alex/pi_bootgen/yocto/meta-bootgen  5
```

## Check build configuration
```
bitbake core-image-minimal -n

uild Configuration:
BB_VERSION           = "1.46.0"
BUILD_SYS            = "x86_64-linux"
NATIVELSBSTRING      = "ubuntu-23.04"
TARGET_SYS           = "aarch64-poky-linux"
MACHINE              = "qemuarm64"
DISTRO               = "poky"
DISTRO_VERSION       = "3.1.27"
TUNE_FEATURES        = "aarch64 armv8a crc"
TARGET_FPU           = ""
meta                 
meta-poky            
meta-yocto-bsp       = "dunfell:df86cc15d0a39d8d85747f7acc2c887cccfd9fa7"
meta-oe              
meta-python          
meta-multimedia      
meta-networking      = "dunfell:b8b0b06821d4d4df0cce4f07fa31a8ca1dd38f46"
meta-raspberrypi     = "dunfell:2081e1bb9a44025db7297bfd5d024977d42191ed"
meta-bootgen         = "main:4a4f60bd483614f7c78cdb3db7a1e3ee1cf7bd6e"
```

## Openssl for aarch64 cross compilation
BOOTGEN require openssl but since we are cross compiling, we need to compile the aarh64 version of it.
```
cd $HOME

wget https://www.openssl.org/source/openssl-1.1.1.tar.gz
tar xzf openssl-1.1.1.tar.gz
```
Install gcc-aarch64-linux-gnu
```
sudo apt-get install gcc-aarch64-linux-gnu

./Configure linux-generic32 shared --prefix=/usr/local/openssl -DOPENSSL_NO_SECURE_MEMORY
```
In the Makefile, go to the CROSS_COMPILE variable and add aarch64-linux-gnu- to the line
```
CROSS_COMPILE=aarch64-linux-gnu-
```
```
make
sudo make install
```
## Build minimal image
Download packages
```
bitbake core-image-minimal --runonly=fetch
```
Build !!
```
bitbake core-image-minimal
```

##Boot the image with qemu
```
runqemu qemuarm64
```
Once booted, enter "root" as username and type "bootgen". 

