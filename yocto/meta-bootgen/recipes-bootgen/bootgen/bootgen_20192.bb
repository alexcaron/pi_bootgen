SUMMARY = "Xilinx bootgen utility"
DESCRIPTION = "Generates boot images for Xilinx devices"
HOMEPAGE = "https://github.com/Xilinx/bootgen"
 
 
BOOTGEN_DIR = "../git"
 
LICENSE = "Apache-2.0 & openssl"
LIC_FILES_CHKSUM = "file://${BOOTGEN_DIR}/LICENSE;md5=c979df673927004a489691fc457facff"
 
SRC_URI = "git://github.com/Xilinx/bootgen.git;protocol=https;tag=2019.2;name=bootgen"
 

S = "${WORKDIR}/git"


do_compile () {
     # Build bootgen
    export CROSS_COMPILER='${CXX}'
    oe_runmake -C ../git "INCLUDE_USER"=-I/usr/local/openssl/include "LIBS=/usr/local/openssl/lib/libssl.a /usr/local/openssl/lib/libcrypto.a -ldl -lpthread"
}
 
do_install() {
      install -d ${D}${bindir}
      install -m 0755 bootgen ${D}${bindir}
}
